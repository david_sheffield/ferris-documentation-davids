Authorization Chains
====================

.. currentmodule:: ferris.core.controller

Ferris uses the concept of authorization chains to control access to :doc:`controllers <controllers>` and their actions. The concept is simple: A authorization chain consists of a series of functions (or callables). When trying to determine to allow or deny a request each function in the chain is called. If any of the functions return False then the request is rejected. Chains are specified using :attr:`Controller.Meta.authorizations <ferris.core.controller.Controller.Meta.authorizations>` and the :func:`add_authorizations` decorator.


Using Authorizations
--------------------

Here's an example of requiring a user to be logged in to access a controller::

    from ferris import Controller

    def require_user(controller):
        return True if controller.user else False

    class Protected(Controller):
        class Meta:
            authorizations = (require_user,)

        ...

You can also use :func:`add_authorizations` instead::

    @route
    @add_authorizations(require_user)
    def protected_action(self):
        ...

.. autofunction:: add_authorizations


Creating Authorization Functions
--------------------------------

As shown above a simple authorization function is rather trivial::

    def require_user(controller):
        return True if controller.user else False


Note that you can also include a message::

    def require_user(controller):
        return True if controller.user else (False, "You must be logged in!")


Built-in Authorization Functions
--------------------------------

.. module:: ferris.core.auth

The module ``ferris.core.auth`` includes some built-in useful authorization functions and utilities.

.. autofunction:: require_user
.. autofunction:: require_admin


There are a few function generators that use predicates. These are useful shortcut authorization functions.

.. autofunction:: require_user_for_prefix(prefix)

    Generates an authorization function that requires that users are logged in for the given prefix.

.. autofunction:: require_admin_for_prefix(prefix)

    Generates an authorization function that requires that the user is an App Engine admin for the given prefix.

.. autofunction:: require_user_for_action
.. autofunction:: require_admin_for_action
.. autofunction:: require_user_for_route
.. autofunction:: require_admin_for_route


You can also create your own generators using precidates. 

.. autofunction:: prefix_predicate
.. autofunction:: action_predicate
.. autofunction:: route_predicate

Then use ``predicate_chain`` to combine them with your authorization function.

.. autofunction:: predicate_chain
